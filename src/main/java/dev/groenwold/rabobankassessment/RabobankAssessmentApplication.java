package dev.groenwold.rabobankassessment;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RabobankAssessmentApplication {

    public static void main(String[] args ){
        SpringApplication.run(RabobankAssessmentApplication.class, args);
    }
}
