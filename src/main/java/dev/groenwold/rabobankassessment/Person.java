package dev.groenwold.rabobankassessment;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Data
@Entity
public class Person {
    private final @Id @GeneratedValue long id;
    private final String firstName;
    private final String lastName;
    private final String dateOfBirth;
    private final String address;

    public Person(long id, String firstName, String lastName, String dateOfBirth, String address){
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.dateOfBirth = dateOfBirth;
        this.address = address;
    }
}
