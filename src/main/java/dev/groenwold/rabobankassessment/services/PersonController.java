package dev.groenwold.rabobankassessment.services;

import dev.groenwold.rabobankassessment.Person;
import dev.groenwold.rabobankassessment.PersonRepository;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class PersonController {

    private final PersonRepository repository;

    public PersonController(PersonRepository repository){
        this.repository = repository;
    }

    @PutMapping("/persons/{id}")
    Person replacePerson(@RequestBody Person newPerson, @PathVariable Long id){
        return repository.save(newPerson);
    }

    @GetMapping("/persons")
    List<Person> all(){
        return repository.findAll();
    }

    @GetMapping("/persons/{id}")
    Person one(@PathVariable Long id){
        return repository.findOne(id);
    }

}
