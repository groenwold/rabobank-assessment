package dev.groenwold.rabobankassessment;

import dev.groenwold.rabobankassessment.Person;
import dev.groenwold.rabobankassessment.PersonRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.util.List;

@Configuration
class LoadDatabase {
    @Bean
    public PersonRepository repository() {
        return new PersonRepository() {
            @Override
            public List<Person> findAll() {
                return null;
            }

            @Override
            public List<Person> findAll(Sort sort) {
                return null;
            }

            @Override
            public List<Person> save(Iterable<? extends Person> iterable) {
                return null;
            }

            @Override
            public void flush() {

            }

            @Override
            public Person saveAndFlush(Person person) {
                return null;
            }

            @Override
            public void deleteInBatch(Iterable<Person> iterable) {

            }

            @Override
            public Page<Person> findAll(Pageable pageable) {
                return null;
            }

            @Override
            public Person save(Person person) {
                return null;
            }

            @Override
            public Person findOne(Long aLong) {
                return null;
            }

            @Override
            public boolean exists(Long aLong) {
                return false;
            }

            @Override
            public long count() {
                return 0;
            }

            @Override
            public void delete(Long aLong) {

            }

            @Override
            public void delete(Person person) {

            }

            @Override
            public void delete(Iterable<? extends Person> iterable) {

            }

            @Override
            public void deleteAll() {

            }
        };
    }
    @Bean
    CommandLineRunner initDatabase(PersonRepository repository) {
        return args -> {
            //Pre-load Person
            repository.save(new Person(1, "Sytse", "Groenwold", "2019-02-17", "Almere"));
        };
    }
}
