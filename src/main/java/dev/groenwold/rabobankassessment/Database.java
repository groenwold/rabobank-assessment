package dev.groenwold.rabobankassessment;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

@Component
public class Database {

    @Autowired
    private Database dataSource;
    private Connection connection = null;
    private Statement statement;

    @PostConstruct
    public void initialize(){
        try {
            Class.forName("org.sqlite.JDBC");
            connection = DriverManager.getConnection("jdbc:sqlite:persistance.sqlite");
            System.out.println("Database: "+ connection.getMetaData().getDriverName());
            statement = connection.createStatement();
            statement.setQueryTimeout(5);

            // Initiale tables
            createTablePerson();
            insertPerson("Sytse", "Groenwold", "2019-02-17", "Almere");
//            createTablePet();
//            insertPet("Sammy", 9);

        } catch (Exception e) {
            e.printStackTrace();
            System.exit(201);
        }
    }

    private void insertPet(String name, int age) {
        try {
            statement.executeUpdate(
                    "INSERT OR IGNORE INTO Pet(Name, Age) " +
                        " VALUES('"+name+"', '"+age+"' " +
                        ");"
            );
        } catch (SQLException e) {
            e.printStackTrace();
            System.exit(204);
        }
    }

    private void insertPerson(String firstName, String lastName, String date, String address) {
        try {
            statement.executeUpdate(
                    "INSERT OR IGNORE INTO Person(FirstName, LastName, DateOfBirth, CurrentAddress) " +
                    "VALUES('"+firstName+"', '"+lastName+"', '"+date+"', '"+address+"' " +
                    ");"
            );
        } catch (SQLException e) {
            e.printStackTrace();
            System.exit(204);
        }
    }

    private void createTablePet() {
        try {
            statement.executeUpdate(
                    "CREATE TABLE IF NOT EXISTS \"Users\" (\n" +
                            "  \"Id\" integer NOT NULL PRIMARY KEY AUTOINCREMENT,\n" +
                            "  \"Name\" text(20,0) NOT NULL,\n" +
                            "  \"Age\" tinyint NOT NULL);");
        } catch (SQLException e) {
            e.printStackTrace();
            System.exit(203);
        }
    }

    private void createTablePerson() {
        try {
            statement.executeUpdate("DROP TABLE Person;");
            statement.executeUpdate(
                    "CREATE TABLE IF NOT EXISTS \"Person\" (\n" +
                            "  \"Id\" integer NOT NULL PRIMARY KEY AUTOINCREMENT,\n" +
                            "  \"FirstName\" text(20,0) NOT NULL,\n" +
                            "  \"LastName\" text(20,0) NOT NULL,\n" +
                            "  \"DateOfBirth\" text(10,0) NOT NULL,\n" +
                            "  \"CurrentAddress\" text(20,0) NOT NULL, \n" +
                            "  UNIQUE(FirstName, LastName) " +
                            ");");

        } catch (SQLException e) {
            e.printStackTrace();
            System.exit(202);
        }
    }


}
