To run the application, simply run RabobankAssessmentApplication.main() method.
Alternatively, launch it through maven with 'mvn clean spring-boot:run'.

Once running, the following commands can be performed through another terminal:
    - All persons:
        curl -v localhost:8080/persons
    - A person by ID:
        curl -v localhost:8080/employees/1
    - Insert a new person:
        $ curl -X POST localhost:8080/employees 
            -H 'Content-type:application/json' 
            -d '{"id": 2,
                 "firstName": "John", 
                 "lastName": "Doe",
                 "dateOfBirth": "1970-01-01",
                 "address": "city"
                }'
    